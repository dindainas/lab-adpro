package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member member1 = new OrdinaryMember("Dinda", "Sister");
        member.addChildMember(member1);
        assertEquals(member.getChildMembers().size(),1);

    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member member1 = new OrdinaryMember("Dinda", "Sister");
        member.addChildMember(member1);
        assertEquals(member.getChildMembers().size(),1);

        member.removeChildMember(member1);
        assertEquals(member.getChildMembers().size(),0);


    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member member1 = new OrdinaryMember("Dinda", "Sister");
        Member member2 = new OrdinaryMember("Siti", "Sister");
        Member member3 = new OrdinaryMember("Thami", "Sister");
        Member member4 = new OrdinaryMember("Inez", "Sister");
        member.addChildMember(member1);
        member.addChildMember(member2);
        member.addChildMember(member3);
        member.addChildMember(member4);

        assertEquals(member.getChildMembers().size(),3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member member1 = new PremiumMember("Dinda", "Master");
        Member member2 = new OrdinaryMember("Siti", "Sister");
        Member member3 = new OrdinaryMember("Thami", "Sister");
        Member member4 = new OrdinaryMember("Inez", "Sister");
        Member member5 = new OrdinaryMember("Ilma", "Sister");
        member1.addChildMember(member2);
        member1.addChildMember(member3);
        member1.addChildMember(member4);
        member1.addChildMember(member5);

        assertEquals(member1.getChildMembers().size(),4);
    }
}
