package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.WeaponProducer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
        weapon2 = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
        weapon3 = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
        weapon4 = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);

        assertEquals(weapon1.getWeaponValue(), 70);
        assertEquals(weapon2.getWeaponValue(), 30);

        weapon1 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon1);
        weapon3 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon4);

        //TODO: Complete me

        assertEquals(weapon1.getWeaponValue(),71);
        assertEquals(weapon3.getWeaponValue(), 15);
        assertEquals(weapon4.getWeaponValue(), 35);

    }

}
