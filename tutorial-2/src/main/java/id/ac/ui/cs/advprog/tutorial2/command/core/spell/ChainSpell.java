package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    ArrayList<Spell> allSpell;

    public ChainSpell(ArrayList<Spell> spells){
        this.allSpell = spells;
    }

    public void cast() {
        for (Spell spl: allSpell){
            spl.cast();
        }
    }

    @Override
    public void undo() {
        for(int i=allSpell.size()-1; i>=0; i--){
            allSpell.get(i).undo();
        }

    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
